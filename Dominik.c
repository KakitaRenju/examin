#include <stdio.h>  // Nom : Dominik Grobecker
#include <stdlib.h>
#include <time.h>         // on utilise cette bibiotheque pour faire appel a l'heure afin d'initialiser des randoms differents a chaque p�rtie
#define PRIXMONTRE 10     //
#define ENVIE 1           //
#define MORT 0            //
#define PRESSEDETENTE 20  //
int roulette(int nbJetons)
{
    int choixJ; // declaration de la variable qui stocke le choix du joueur : pair rouge ou impair noire
    int caseRoulette; // declaration de la variable du resultat du lancer de roulette
    int mise; // declaration de la variable qui stocke la mise du joueur

    printf("\nVous jouez a la roulette\n"); // annonce qu'on joue � la roulette

    while ((nbJetons>0) && (nbJetons<100)) // tant que les jeton ne sont pas a 0 ou a plus 100 on continue de jouer � la roulette
    {

        do // tant que le joueur mise plus que possible on lui redemande combien il mise
        {
            printf("Combien misez vous? (vous ne pouvez pas miser plus de 25 ou que votre nombre de jetons)\n");
            fseek(stdin,0,SEEK_END); // mise a la fin du buffer afin eviter les entr�e multiple en une saisies
            scanf("%d",&mise); // on demande au joueur son choix
        }
        while ((mise>nbJetons) || (mise>25) || (mise<1)); // boucle pour forcer a rentrer un choix correct
        do // on s'assure que le nombre soit 1 ou 0
        {
            printf("Vous pariez sur les rouges ou les noirs\n0 pour Rouge (Pair)\n1 pour Noire (Impair)\n"); // le joueur rentre son choix
            fseek(stdin,0,SEEK_END); // mise a la fin du buffer afin eviter les entr�es multiples en une saisies
            scanf("%d",&choixJ); // on demande au joueur son choix
        }
        while ((choixJ<0) || (choixJ>1)); // boucle pour forcer a rentrer un choix correct


        caseRoulette=rand()%37; // generation aleatoire de resultat de la roulette compris entre 0 et 36

        if (caseRoulette==0) // si la roulette tombe sur 0 le joueur perd sa mise
        {
            nbJetons=nbJetons-mise; // le joueur perd sa mise
            printf("Le resultat de la roulette est la case %d,, vous perdez votre mise \n vous avez %d jetons\n",caseRoulette,nbJetons);
        }
        else
        {
            if ((caseRoulette%2==0) && (choixJ==0)) // si le jouer parie rouge et que la boulle tombe sur une case rouge il gagne sa mise
            {
                nbJetons=nbJetons+mise; // le joueur recupere sa mise et gagne sa mise
                printf("Le resultat de la roulette est rouge case numero %d, vous gagnez votre mise \n vous avez %d jetons \n",caseRoulette,nbJetons);
            }
            else
            {
                if ((caseRoulette%2==1) && (choixJ==1)) // si le jouer parie noire et que la boulle tombe sur noire il gagne sa mise
                {
                    nbJetons=nbJetons+mise; // le joueur gagne sa mise
                    printf("Le resultat de la roulette est noire case numero %d, vous gagnez votre mise \nvous avez %d jetons\n",caseRoulette,nbJetons);
                }
                else // les autres cas le joueurs perd
                {
                    nbJetons=nbJetons-mise; // le joueur recupere sa mise et gagne sa mise
                    printf("Le resultat de la roulette est le numero %d, vous perdez votre mise \n vous avez %d jetons\n",caseRoulette,nbJetons);
                }
            }
        }
    }
    return nbJetons; // retourn les gains de la cession de roulette au main
}

int rouletteRusse() // fonction roulette russe
{
    int nbJetons;     // declaration de la variable qui est le nombre de jetons gagn�
    int barillet[6]; //declaration de la variable tableau de 6 chambres
    int i=0;                    // declaration de l'iterateur

    printf("\nVous jouez a la roulette russe !!\n"); // annonce qu'on joue � la roulette Russe

    for (i=0; i<6; i++) // on cr�e/vide toutes les chambres du barillet
    {
        barillet[i]=0;
    }
    barillet[rand()%6]=1; // on insert la balle dans une chambre aleatoire
    nbJetons=tirRouletteRusse(barillet); // on appelle la fonction tirRouletteRusse et les gains retourn�s sont stockes dans nbJetons (0 = la mort du joueur)
    if (nbJetons==0)
    {
        return 0; // retourne 0 pour indiquer que le jouer est mort
    }
    else
    {
        return nbJetons;// sinon retourne le nb de jetons gagn� dans la fonction tirRouletteRusse
    }
}



int tirRouletteRusse(int barillet[6]) // la fonction tir roulette russe
{
    int nbAppuyDetente=0; // initialisation du nombre de tir d'affil� que le jouer fait
    int retirer; // variable pour savoir si le joueur souhaite continu� a tirer
    int i=0; // declaration de l'iterateur
    do
    {
        if (barillet[0]!=0) // si la chambre etait vide
        {
            printf("Tu appuyes sur la gachette\nBoom, t'es mort !\n"); // on indique au joueur qu'il est mort
            return 0; //on retourne 0 pour la mort
        }
        else
        {
            printf("Tu appuyes sur la gachette\nLa chambre du pistolet etait vide \n"); // on indique au joueueur qu'il a survecu le 1er tir
            do // on s'assure que le nombre soit 1 ou 0
            {
                printf("Voulez vous retirer ?\n0 pour non\n1 pour oui\n"); // on demande au joueur si il souhaite retirer
                fseek(stdin,0,SEEK_END);
                scanf("%d",&retirer);
            }
            while ((retirer<0) || (retirer>1)); // boucle pour forcer a rentrer un choix correct

            nbAppuyDetente++ ; // on incremente le nombre de tir effectuier par le joueur pour definir ces gains

            for (i=0; i<=5; i++)
            {
                if (i==5)
                {
                    barillet[5]=0; //vu qu'un tir a ete effctuer la derniere chambre est forcement vide
                }
                else
                {
                    barillet[i]=barillet[i+1]; //on fait tourner le barillet
                }
            }
        }
    }
    while (retirer==1); // si le joueur souhaite retirer pour gagner plus on recommence

    printf("Vous avez gagner %d Jetons en jouant a la roulette russe\n",PRESSEDETENTE*nbAppuyDetente); // on indique les gains du joueur (20* nombre d'appuye de la gachette appuyer au jeux) jetons de la roulette russe
    return PRESSEDETENTE*nbAppuyDetente; // on retourne les gains
}

int main()
{

    int nbJetons=PRIXMONTRE; // le prix de sa montre convertit en jetons
    int vie=ENVIE; // le joueur commence la partie en vie

    srand(time(NULL)); // initialisation du random grace a l'heure et la date

    while (nbJetons<100 && vie==1) // je jeu continu tant que le joueur vie et tant qu'il n'atteint pas 100 jetons
    {
        nbJetons=roulette(nbJetons); // on appele la fonction roulette et on stock les gains dans nbJetons

        if (nbJetons>=100) // cas du joueur gagnant
        {
            printf("Vous sortez vivant du casino !!\n");
            if (nbJetons>=110) // cas ou le jouer rachete sa montre
            {
                printf("Et vous avez repris votre Rolex !\nParceque si a 50 ans, on a pas de rolex on a rate sa vie !\n");
            }
        }
        if (nbJetons<=0) // si le joueur � 0 jetons il est oblig� de jouer a la roulette russe.
        {
            nbJetons=rouletteRusse(); // on lance le jeu roulette russe et on stock les gains ou la mort si 0
            if (nbJetons ==0) // si les gains de la roulette russe sont nul c'est que le joueur est mort
            {
                printf("Vous sortez du casino les pieds devant !\n"); // decription narrative du game over
                vie=MORT; // on sort de la boucle c'est la fin du jeu
            }
        }
    }
    return 0;
}
